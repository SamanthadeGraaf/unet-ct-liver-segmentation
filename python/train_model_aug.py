import tensorflow as tf 
import os
import random
import numpy as np
from losses import *
from tqdm import tqdm

from skimage.io import imread, imshow
from skimage.transform import resize
import matplotlib.pyplot as plt

# 1. Set path where to save model, weights and figures
MODEL_PATH = '/data/scratch/sdegraaf/train_model/models/'
model_name = 'model_name'
weight_name = 'weights_name'
FIGURES_PATH = '/data/scratch/sdegraaf/train_model/figures/'
figure_name = 'figure_name'

batch_size = 8
epochs = 100

# 2. Set input format images
IMG_HEIGHT = 256
IMG_WIDTH = 256
IMG_CHANNELS = 1 #greyscale

# 3. Set path to png data (has to be saved in certain order, see data_sort.py)
TRAIN_PATH = '/data/scratch/sdegraaf/png_datasets/train/'
TEST_PATH = '/data/scratch/sdegraaf/png_datasets/test/'
VAL_PATH = '/data/scratch/sdegraaf/png_datasets/val/'

train_ids = next(os.walk(TRAIN_PATH))[1]
test_ids = next(os.walk(TEST_PATH))[1]
val_ids = next(os.walk(VAL_PATH))[1]

# Create training - val - test samples
X_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
Y_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.float32)
X_val = np.zeros((len(val_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
Y_val= np.zeros((len(val_ids), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.float32)

print('Preparing training samples..')
for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
    path = TRAIN_PATH + id_
    img = imread(path + '/image/' + id_ + '.png')
    img = img.reshape((img.shape[0], img.shape[1], 1))[:, :, :IMG_CHANNELS]
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    X_train[n] = img  
    
    mask = imread(path + '/mask/' + id_ + '.png')
    mask = mask.reshape((mask.shape[0], mask.shape[1], 1))[:, :, :IMG_CHANNELS]
    mask = resize(mask, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    Y_train[n] = mask
#print(X_train.dtype) 
#print(Y_train.dtype) 

# Perform data augmentation on training images
data_gen_args = dict(rotation_range=2,
                     width_shift_range=0.05,
                     height_shift_range=0.05,
                     zoom_range=0.2)
image_datagen = tf.keras.preprocessing.image.ImageDataGenerator(**data_gen_args)
mask_datagen = tf.keras.preprocessing.image.ImageDataGenerator(**data_gen_args)
# Provide the same seed and keyword arguments to the fit and flow methods
seed = 1
image_datagen.fit(X_train, augment=True, seed=seed)
mask_datagen.fit(Y_train, augment=True, seed=seed)
image_generator = image_datagen.flow(
    X_train,
    batch_size = batch_size,
    seed=seed,
    shuffle = True)
    #save_to_dir= '/data/scratch/sdegraaf/view_augmented_images',
    #save_prefix="view_augmented_images",
    #save_format="png")
mask_generator = mask_datagen.flow(
    Y_train,
    batch_size = batch_size,
    seed=seed,
    shuffle = True)
    #save_to_dir= '/data/scratch/sdegraaf/view_augmented_images',
    #save_prefix="view_augmented_masks",
    #save_format="png")
# combine generators into one which yields image and masks
train_generator = zip(image_generator, mask_generator)

print('Preparing validation samples..')
for n, id_ in tqdm(enumerate(val_ids), total=len(val_ids)):
    path = VAL_PATH + id_
    img = imread(path + '/image/' + id_ + '.png')
    img = img.reshape((img.shape[0], img.shape[1], 1))[:, :, :IMG_CHANNELS]
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    X_val[n] = img  
    mask = np.zeros((IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool)
    for mask_file in next(os.walk(path + '/mask/'))[2]:
        mask_ = imread(path + '/mask/' + mask_file)
        mask_ = np.expand_dims(resize(mask_, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True), axis=-1)
        mask = np.maximum(mask, mask_)

    Y_val[n] = mask
#print(Y_val.dtype) 

print('Preparing test samples..')
X_test = np.zeros((len(test_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
sizes_test = []
print('Resizing test images')
for n, id_ in tqdm(enumerate(test_ids), total=len(test_ids)):
    path = TEST_PATH + id_
    img = imread(path + '/image/' + id_ + '.png')
    img = img.reshape((img.shape[0], img.shape[1], 1))[:, :, :IMG_CHANNELS]
    sizes_test.append([img.shape[0], img.shape[1]])
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    X_test[n] = img

print('Done!')

# CHECK: Perform checks if the masks of the training samples match the image
#print('Shape of training set:'+ ' image ' + str(X_train.shape) + ', mask ' + str(Y_train.shape))
#print('Shape of validating set:'+ ' image ' + str(X_val.shape) + ', mask ' + str(Y_val.shape))
#image_x = random.randint(0, len(train_ids))
#f = plt.figure()
#plt.title('Check on random training sample')
#f.add_subplot(1, 2, 1)
#plt.imshow(X_train[image_x], cmap = 'gray')
#f.add_subplot(1, 2, 2)
#plt.imshow(np.squeeze(Y_train[image_x]), cmap='gray')
#plt.savefig(FIGURES_PATH+ 'check_samples.png')
#plt.show()

## Build U-Net
inputs = tf.keras.layers.Input((IMG_WIDTH, IMG_HEIGHT, IMG_CHANNELS))
inputs = tf.keras.layers.Lambda(lambda x: x/255)(inputs)
dropout = 0.5

# Contracting path 
c1 = tf.keras.layers.Conv2D(32, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
c1 = tf.keras.layers.Conv2D(32, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(c1)
p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1) 
p1 = tf.keras.layers.Dropout(dropout)(p1)

c2 = tf.keras.layers.Conv2D(64, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(p1)
c2 = tf.keras.layers.Conv2D(64, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(c2)
p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2) 
p2 = tf.keras.layers.Dropout(dropout)(p2)

c3 = tf.keras.layers.Conv2D(128, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(p2)
c3 = tf.keras.layers.Conv2D(128, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(c3)
p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3) 
p3 = tf.keras.layers.Dropout(dropout)(p3)

c4 = tf.keras.layers.Conv2D(256, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(p3)
c4 = tf.keras.layers.Conv2D(256, (3,3), activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(c4)

# Expanding path 
u5 = tf.keras.layers.Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(c4)
u5 = tf.keras.layers.concatenate([u5, c3])
c5 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u5)
c5 = tf.keras.layers.Dropout(dropout)(c5)
c5 = tf.keras.layers.Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c5)

u6 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c5)
u6 = tf.keras.layers.concatenate([u6, c2])
c6 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u6)
c6 = tf.keras.layers.Dropout(dropout)(c6)
c6 = tf.keras.layers.Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c6)

u7 = tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c6)
u7= tf.keras.layers.concatenate([u7, c1])
c7 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(u7)
c7 = tf.keras.layers.Dropout(dropout)(c7)
c7 = tf.keras.layers.Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same')(c7)

norm = tf.keras.layers.BatchNormalization(axis=-1, momentum=0.9, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None)(c7)
outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(norm)
model = tf.keras.Model(inputs, outputs)
model.compile(optimizer='adam', loss=DiceCoefLoss, metrics=[DiceCoef,'accuracy']) # learning rate defaults to 1 E-4
model.summary()


# Modelcheckpoint
checkpointer = tf.keras.callbacks.ModelCheckpoint(MODEL_PATH + model_name+ '_model.h5', verbose=1, save_best_only=True) 
    tf.keras.callbacks.EarlyStopping(patience=30, monitor='loss'), 
    checkpointer]       
# Modelfit
lentrain = len(train_ids)
steps_per_epoch = lentrain/batch_size
history = model.fit(train_generator,
	validation_data = (X_val, Y_val), 
	batch_size=8, 
	shuffle = True, 
	epochs=epochs, 
	steps_per_epoch = steps_per_epoch,
	callbacks=callbacks)

# Save model and weight files
model_json = model.to_json()
with open(MODEL_PATH + model_name + ".json", "w") as json_file:
    json_file.write(model_json)
model.save_weights(MODEL_PATH + weight_name+".h5")
print("Model + weights saved")

# list all data in history
print(history.history.keys())

# Loss history plot
plt.figure()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title(figure_name + ':loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.savefig(FIGURES_PATH+ figure_name+ '_loss.png')
#plt.show()

# Accuracy history plot
plt.figure()
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title(figure_name + ':accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.savefig(FIGURES_PATH+ figure_name +'_accuracy.png')
#plt.show()
