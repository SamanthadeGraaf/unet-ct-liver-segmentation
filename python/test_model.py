import tensorflow as tf 
import os
import random
import numpy as np
from tqdm import tqdm
from skimage.io import imread, imshow
from skimage.transform import resize
import matplotlib.pyplot as plt
from skimage.io import imread, imshow

# 1. Load model
MODEL_PATH = 'C:/Users/Samantha/Documents/BIGR_Internship/models/model.H5'
model = tf.keras.models.load_model(MODEL_PATH, compile = False)

# 2. Path to save images
FIGURES_PATH = 'C:/Users/Samantha/Documents/BIGR_Internship/figures/'
figure_name = 'figure_name'

# 3. Set path to png data (has to be saved in certain order, see data_sorted.py)
#image = 'empty/'
image = 'liver/'
#image = 'liver+heart/'
TEST_PATH = 'C:/Users/Samantha/Documents/BIGR_Internship/DATA/test/' + image

# That's it!

# Loading in test data
IMG_HEIGHT = 256
IMG_WIDTH = 256
IMG_CHANNELS = 1 #greyscale

test_ids = next(os.walk(TEST_PATH))[1]
X_test = np.zeros((len(test_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
Y_test = np.zeros((len(test_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
sizes_test = []
print('Resizing test images')
for n, id_ in tqdm(enumerate(test_ids), total=len(test_ids)):
    path = TEST_PATH + id_
    img = imread(path + '/image/' + id_ + '.png')
    img = img.reshape((img.shape[0], img.shape[1], 1))[:, :, :IMG_CHANNELS]
    sizes_test.append([img.shape[0], img.shape[1]])
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    X_test[n] = img
    mask = imread(path + '/mask/' + id_ + '.png')
    mask = mask.reshape((mask.shape[0], mask.shape[1], 1))[:, :, :IMG_CHANNELS]
    mask = resize(mask, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    Y_test[n] = mask
print('Done!')


#  Predicted mask from testset
preds_test1 = model.predict(X_test[:int(X_test.shape[1]*0.5)], verbose=1)
preds_test2 = model.predict(X_test[:int(X_test.shape[0]*0.5)], verbose=1)
#  Threshold predictions to binarize the image
preds_test_t1 = (preds_test1 > 0.8).astype(np.uint8)
preds_test_t2 = (preds_test2 > 0.8).astype(np.uint8)


#  Select random test samples to show
ix = random.randint(0, len(preds_test_t1)-1)
ix2 = random.randint(0, len(preds_test_t1)-1)
g = plt.figure()
g.add_subplot(2, 3, 1)
plt.imshow(X_test[ix], cmap = 'gray')
plt.title('Image')
g.add_subplot(2, 3, 2)
imshow(np.squeeze(Y_test[ix]), cmap = 'gray')
plt.title('Mask')
g.add_subplot(2, 3, 3)
plt.title('Predicted mask')
plt.imshow(np.squeeze(preds_test_t1[ix]), cmap='gray')
g.add_subplot(2, 3, 4)
plt.imshow(X_test[ix2], cmap = 'gray')
plt.title('Image')
g.add_subplot(2, 3, 5)
imshow(np.squeeze(Y_test[ix2]), cmap = 'gray')
plt.title('Mask')
g.add_subplot(2, 3, 6)
plt.title('Predicted mask')
plt.imshow(np.squeeze(preds_test_t2[ix2]), cmap='gray')
plt.savefig(FIGURES_PATH + figure_name +'.png', bbox_inches='tight')
plt.show()


