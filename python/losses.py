from keras.losses import binary_crossentropy
import keras.backend as K
import tensorflow as tf

def DiceCoef(y_true, y_pred):
	y_true_f = K.flatten(y_true)
	y_pred_f = K.flatten(y_pred)
	intersection = K.sum(y_true_f*y_pred_f)
	return (2.*intersection)/(K.sum(y_true_f) + K.sum(y_pred_f) + 0.00001)

def DiceCoefLoss(y_true, y_pred):
	return 1-DiceCoef(y_true, y_pred)

def DiceCoef2(y_true, y_pred):
	y_true_f = K.flatten(y_true)
	y_pred_f = K.flatten(y_pred)
	intersection = K.sum(y_true_f*y_pred_f)
	return (2.*intersection+0.00001)/(K.sum(y_true_f) + K.sum(y_pred_f) + 0.00001)

def DiceCoefLoss2(y_true, y_pred):
	return 1-DiceCoef2(y_true, y_pred)