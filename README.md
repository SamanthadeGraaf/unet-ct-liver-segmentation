This file contains short explanations of the files within this folder.

* data/..: manual augmentations performed on raw datasets

* python/data_sort.py: preparation of data, sorts data in order before training
* python/losses.py: program called in train_model.py and train_model_aug.py, containing evaluation measures
* python/test_model.py: tests the trained model 
* python/train_model.py: how to train the model
* python/train_model_aug.py: how to train the model with data augmentation
